<?php

namespace PdoReplay\Test;

use PDO;
use PdoReplay\PdoRecorder;
use PdoReplay\PdoReplay;
use PHPUnit\Framework\TestCase;

class ReplayTest extends TestCase
{
    private $upstreamPdo;

    protected function setUp(): void
    {
        parent::setUp();

        $this->upstreamPdo = new PDO("sqlite::memory:");
        $this->upstreamPdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->upstreamPdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }

    public function test_fetch()
    {
        $replayFile = __DIR__ . '/test_fetch.replay.php';

        $pdoRecorder = new PdoRecorder($this->upstreamPdo, $replayFile);
        $stmt = $pdoRecorder->query('SELECT :name as name');
        $stmt->execute(['name' => 'adam']);
        $stmt->fetch();

        $pdoReplay = new PdoReplay($replayFile);
        $stmt = $pdoReplay->query('SELECT :name as name');
        $stmt->execute(['name' => 'adam']);
        $result = $stmt->fetch();

        $this->assertEquals(['name' => 'adam', 0 => 'adam'], $result);
    }


    public function test_fetch_multiple_records()
    {
        $replayFile = __DIR__ . '/test_fetch_multiple_records.replay.php';

        $this->upstreamPdo->exec('CREATE TABLE people(name VARCHAR)');
        $this->upstreamPdo->exec('INSERT INTO people VALUES("adam"), ("peter")');

        $pdoRecorder = new PdoRecorder($this->upstreamPdo, $replayFile);
        $stmt = $pdoRecorder->query('SELECT name FROM people');
        $stmt->execute();
        $stmt->fetch();
        $stmt->fetch();

        $pdoReplay = new PdoReplay($replayFile);
        $stmt = $pdoReplay->query('SELECT name FROM people');
        $stmt->execute();

        $this->assertEquals(['name' => 'adam', 0 => 'adam'], $stmt->fetch());
        $this->assertEquals(['name' => 'peter', 0 => 'peter'], $stmt->fetch());
    }

    public function test_exec()
    {
        $replayFile = __DIR__ . '/test_exec.replay.php';
        $this->upstreamPdo->exec('CREATE TABLE people(name VARCHAR)');

        $pdoRecorder = new PdoRecorder($this->upstreamPdo, $replayFile);
        $pdoRecorder->exec('INSERT INTO people VALUES("adam"), ("peter")');

        $pdoReplay = new PdoReplay($replayFile);
        $result = $pdoReplay->exec('INSERT INTO people VALUES("adam"), ("peter")');

        $this->assertEquals(2, $result);
    }

    public function test_bindValue()
    {
        $replayFile = __DIR__ . '/test_bindValue.replay.php';
        $pdoRecorder = new PdoRecorder($this->upstreamPdo, $replayFile);
        $stmt = $pdoRecorder->prepare('SELECT :param');
        $stmt->bindValue('param', 'hello', PDO::PARAM_STR);
        $stmt->execute();
        $stmt->fetchColumn();

        $pdoReplay = new PdoReplay($replayFile);
        $stmt = $pdoReplay->prepare('SELECT :param');
        $stmt->bindValue('param', 'hello', PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchColumn();

        $this->assertEquals('hello', $result);
    }

    public function test_statement_execute()
    {
        $replayFile = __DIR__ . '/test_statement_execute.replay.php';
        $this->upstreamPdo->exec('CREATE TABLE people(name VARCHAR)');

        $pdoRecorder = new PdoRecorder($this->upstreamPdo, $replayFile);
        $stmt = $pdoRecorder->prepare('INSERT INTO people VALUES("adam"), ("peter")');
        $stmt->execute();
        $stmt->rowCount();

        $pdoReplay = new PdoReplay($replayFile);
        $stmt = $pdoReplay->prepare('INSERT INTO people VALUES("adam"), ("peter")');
        $result = [
            $stmt->execute(),
            $stmt->rowCount()
        ];

        $this->assertEquals([true, 2], $result);
    }


}
