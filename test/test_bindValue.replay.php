<?php return [
    '4a82115262db43735f84d92a7701a0f0' => [
        'query' => [
            'queryParams' => [
                'SELECT :param',
            ],
            'executeParams' => null,
            'fetchMethod' => 'execute',
            'fetchParams' => [],
            'boundValues' => [
                'param' => [
                    'value' => 'hello',
                    'type' => 2,
                ],
            ],
        ],
        'position' => 0,
        'results' => [
            true,
        ],
    ],
    '0aaf4ab748768c20796643eaec98e23d' => [
        'query' => [
            'queryParams' => [
                'SELECT :param',
            ],
            'executeParams' => null,
            'fetchMethod' => 'fetchColumn',
            'fetchParams' => [],
            'boundValues' => [
                'param' => [
                    'value' => 'hello',
                    'type' => 2,
                ],
            ],
        ],
        'position' => 0,
        'results' => [
            'hello',
        ],
    ],
];