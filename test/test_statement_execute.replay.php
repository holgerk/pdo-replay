<?php return [
    'efa5a0bd457878ca045e7a9d02271906' => [
        'query' => [
            'queryParams' => [
                'INSERT INTO people VALUES("adam"), ("peter")',
            ],
            'executeParams' => null,
            'fetchMethod' => 'execute',
            'fetchParams' => [],
            'boundValues' => [],
        ],
        'position' => 0,
        'results' => [
            true,
        ],
    ],
    '3cd746f7ae84c46b72d2ea21fa46efba' => [
        'query' => [
            'queryParams' => [
                'INSERT INTO people VALUES("adam"), ("peter")',
            ],
            'executeParams' => null,
            'fetchMethod' => 'rowCount',
            'fetchParams' => [],
            'boundValues' => [],
        ],
        'position' => 0,
        'results' => [
            2,
        ],
    ],
];