<?php


namespace PdoReplay;


use PDO;
use PDOStatement;
use Symfony\Component\VarExporter\VarExporter;

class PdoRecorder extends PDO
{
    private PDO $upstream;
    private string $replayFile;

    public function __construct(PDO $upstream, string $replayFile)
    {
        $this->upstream = $upstream;
        $this->replayFile = $replayFile;

        if (file_exists($this->replayFile)) {
            unlink($this->replayFile);
        }
    }

    /**
     * @inheritDoc
     */
    public function prepare($query, $options = []): PDOStatement|false
    {
        $result = $this->upstream->prepare($query, $options);
        if (is_bool($result)) {
            return $result;
        }
        return new StatementRecorder($result, $this, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function beginTransaction(): bool
    {
        return $this->upstream->beginTransaction();
    }

    /**
     * @inheritDoc
     */
    public function commit(): bool
    {
        return $this->upstream->commit();
    }

    /**
     * @inheritDoc
     */
    public function rollBack(): bool
    {
        return $this->upstream->rollBack();
    }

    /**
     * @inheritDoc
     */
    public function inTransaction(): bool
    {
        return $this->upstream->inTransaction();
    }

    /**
     * @inheritDoc
     */
    public function setAttribute($attribute, $value): bool
    {
        return $this->upstream->setAttribute($attribute, $value);
    }

    /**
     * @inheritDoc
     */
    public function exec($statement): false|int
    {
        $result = $this->upstream->exec($statement);
        $this->recordResult($result, 'exec', $statement);
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function query(string $statement, ?int $mode = null, mixed ...$fetchModeArgs): PDOStatement|false
    {
        $pdoStatement = call_user_func_array([$this->upstream, 'query'], func_get_args());
        return new StatementRecorder($pdoStatement, $this, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function lastInsertId($name = null): string|false
    {
        return $this->upstream->lastInsertId($name);
    }

    /**
     * @inheritDoc
     */
    public function errorCode(): ?string
    {
        return $this->upstream->errorCode();
    }

    /**
     * @inheritDoc
     */
    public function errorInfo(): array
    {
        return $this->upstream->errorInfo();
    }

    /**
     * @inheritDoc
     */
    public function getAttribute($attribute): mixed
    {
        return $this->upstream->getAttribute($attribute);
    }

    /**
     * @inheritDoc
     */
    public function quote($string, $type = PDO::PARAM_STR): string|false
    {
        return $this->upstream->quote($string, $type);
    }

    /**
     * @inheritDoc
     */
    public function sqliteCreateFunction($function_name, $callback, $num_args = -1, $flags = 0): bool
    {
        return $this->upstream->sqliteCreateFunction($function_name, $callback, $num_args, $flags);
    }

    public function recordResult($result, $fetchMethod, $fetchParams, $queryParams = null, $executeParams = null, $boundValues = []): void
    {
        if (!file_exists($this->replayFile)) {
            file_put_contents($this->replayFile, '<?php return [];');
        }
        $data = include $this->replayFile;

        // create key
        list($key, $md5Key) = self::getRecordingKey($queryParams, $executeParams, $fetchMethod, $fetchParams, $boundValues);

        // append record
        $data[$md5Key]['query'] = $key;
        $data[$md5Key]['position'] = 0;
        $data[$md5Key]['results'][] = $result;

        file_put_contents($this->replayFile, '<?php return ' . VarExporter::export($data) . ';');
    }

    /**
     * @throws \JsonException
     */
    public static function getRecordingKey($queryParams, $executeParams, $fetchMethod, $fetchParams, $boundValues): array
    {
        $key = [
            'queryParams' => $queryParams,
            'executeParams' => $executeParams,
            'fetchMethod' => $fetchMethod,
            'fetchParams' => $fetchParams,
            'boundValues' => $boundValues,
        ];
        $md5Key = md5(json_encode($key, JSON_THROW_ON_ERROR));
        return [$key, $md5Key];
    }

}
