<?php


namespace PdoReplay;


use PDO;
use PDOStatement;
use RuntimeException;

class StatementRecorder extends PDOStatement
{
    private PDOStatement $upstream;
    private PdoRecorder $pdoRecorder;
    private array $queryParams;

    private ?array $executeParams;
    private array $boundValues = [];

    public function __construct(PDOStatement $upstream, PdoRecorder $pdoRecorder, $queryParams = [])
    {
        $this->upstream = $upstream;
        $this->pdoRecorder = $pdoRecorder;
        $this->queryParams = $queryParams;

        $this->executeParams = null;
        $this->boundValues = [];
    }

    /**
     * @inheritDoc
     */
    public function execute($params = null): bool
    {
        $this->executeParams = $params;
        $result = $this->upstream->execute($params);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetch($mode = PDO::FETCH_BOTH, $cursorOrientation = PDO::FETCH_ORI_NEXT, $cursorOffset = 0): mixed
    {
        $result = $this->upstream->fetch($mode, $cursorOrientation, $cursorOffset);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetchColumn($column = 0): mixed
    {
        $result = $this->upstream->fetchColumn($column);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetchAll(int $mode = PDO::FETCH_BOTH, mixed ...$args): array
    {
        $result = $this->upstream->fetchAll($how = null, $class_name = null, $ctor_args = null);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function fetchObject($class_name = null, $constructorArgs = null): object|false
    {
        $result = $this->upstream->fetchObject($class_name, $constructorArgs);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function rowCount(): int
    {
        $result = $this->upstream->rowCount();
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function columnCount(): int
    {
        $result = $this->upstream->columnCount();
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getColumnMeta($column): array|false
    {
        $result = $this->upstream->getColumnMeta($column);
        $this->recordResult($result, __FUNCTION__, func_get_args());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function bindParam($param, &$var, $type = PDO::PARAM_STR, $length = null, $driverOptions = null): bool
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function bindColumn($column, &$var, $type = null, $maxLength = null, $driverOptions = null): bool
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function bindValue($param, $value, $type = PDO::PARAM_STR): bool
    {
        $this->boundValues[$param] = ['value' => $value, 'type' => $type];
        return $this->upstream->bindValue($param, $value, $type);
    }

    /**
     * @inheritDoc
     */
    public function errorCode(): ?string
    {
        return $this->upstream->errorCode();
    }

    /**
     * @inheritDoc
     */
    public function errorInfo(): array
    {
        return $this->upstream->errorInfo();
    }

    /**
     * @inheritDoc
     */
    public function setAttribute($attribute, $value): bool
    {
        return $this->upstream->setAttribute($attribute, $value);
    }

    /**
     * @inheritDoc
     */
    public function getAttribute($attribute): mixed
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function setFetchMode(int $mode, mixed ...$args): bool
    {
        return $this->upstream->setFetchMode($mode, ...$args);
    }

    /**
     * @inheritDoc
     */
    public function nextRowset(): bool
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function closeCursor(): bool
    {
        return $this->upstream->closeCursor();
    }

    private function recordResult($result, string $method, array $fetchParams = []): void
    {
        $this->pdoRecorder->recordResult($result, $method, $fetchParams, $this->getQueryParams(), $this->getExecuteParams(), $this->boundValues);
    }

    private function getQueryParams()
    {
        return $this->queryParams;
    }

    private function getExecuteParams(): ?array
    {
        return $this->executeParams;
    }

}