<?php


namespace PdoReplay;


use PDO;
use PDOStatement;
use RuntimeException;

class StatementReplay extends PDOStatement
{
    private PdoReplay $pdoReplay;
    private array $queryParams;

    private ?array $executeParams;
    private array $boundValues;

    public function __construct(PdoReplay $pdoReplay, array $queryParams)
    {
        $this->pdoReplay = $pdoReplay;
        $this->queryParams = $queryParams;

        $this->executeParams = null;
        $this->boundValues = [];
    }

    /**
     * @inheritDoc
     */
    public function execute(?array $params = null): bool
    {
        $this->executeParams = $params;
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function fetch(int $mode = PDO::FETCH_BOTH, $cursorOrientation = PDO::FETCH_ORI_NEXT, $cursorOffset = 0): mixed
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function fetchColumn(int $column = 0): mixed
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function fetchAll(int $mode = PDO::FETCH_BOTH, mixed ...$args): array
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function fetchObject($class = "stdClass", $constructorArgs = null): object|false
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function rowCount(): int
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function columnCount(): int
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function getColumnMeta(int $column): array|false
    {
        return $this->replayResult(__FUNCTION__, func_get_args());
    }

    /**
     * @inheritDoc
     */
    public function bindParam($param, &$var, $type = PDO::PARAM_STR, $maxLength = null, $driverOptions = null): bool
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function bindColumn($column, &$var, $type = null, $maxLength = null, $driverOptions = null): bool
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function bindValue($param, $value, $type = PDO::PARAM_STR): bool
    {
        $this->boundValues[$param] = ['value' => $value, 'type' => $type];
        return true;
    }

    /**
     * @inheritDoc
     */
    public function errorCode(): ?string
    {
        // noop
    }

    /**
     * @inheritDoc
     */
    public function errorInfo(): array
    {
        // noop
    }

    /**
     * @inheritDoc
     */
    public function setAttribute($attribute, $value): bool
    {
        // noop
    }

    /**
     * @inheritDoc
     */
    public function getAttribute(int $name): mixed
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function setFetchMode(int $mode, mixed ...$args)
    {
        // noop
    }

    /**
     * @inheritDoc
     */
    public function nextRowset(): bool
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    /**
     * @inheritDoc
     */
    public function closeCursor(): bool
    {
        // noop
    }

    /**
     * @inheritDoc
     */
    public function debugDumpParams(): ?bool
    {
        throw new RuntimeException('The method: ' . __FUNCTION__ . ' is not supported!');
    }

    private function replayResult(string $fetchMethod, array $fetchParams = [])
    {
        return $this->pdoReplay->replayResult($fetchMethod, $fetchParams, $this->getQueryParams(), $this->getExecuteParams(), $this->boundValues);
    }

    private function getQueryParams(): array
    {
        return $this->queryParams;
    }

    private function getExecuteParams(): ?array
    {
        return $this->executeParams;
    }

}